#ifndef __MyCppGame__StageSelectLayer__
#define __MyCppGame__StageSelectLayer__

#include "cocos2d.h"

class StageSelectLayer : public cocos2d::Layer
{
public:
    virtual bool init();
    CREATE_FUNC(StageSelectLayer);
};

#endif /* defined(__MyCppGame__StageSelectLayer__) */

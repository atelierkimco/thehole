#ifndef __STAGE_04_H__
#define __STAGE_04_H__

#include "cocos2d.h"
#include "StageBaseScene.h"

class Stage04 : public StageBaseScene
{
public:
    static cocos2d::Scene* createScene();
    virtual bool init();
    CREATE_FUNC(Stage04);
private:
    enum LAYER_LAP
    {
        ROOM = 0,
        SECLET,
        BALL,
        BALL_SHADOW,
        SECRET,
        MANHOLE,
        CLEAR_BTN,
        SECRET_BTN
    };
    enum MANHOLE_STATUS
    {
        NOMAL = 0,
        FELL
    };
    enum SECRET_BTN_STATUS
    {
        SECRET_HIDDEN = 0,
        SHOW_SECRET,
        GOT_SECRET_OPEN,
        GOT_SECRET_CLOSE
    };
    
    void onAcceleration(cocos2d::Acceleration* acc, cocos2d::Event* unused_event);
    void touchEvent(Ref* pSender, cocos2d::ui::Widget::TouchEventType type);
    virtual void update(float dt);
    void manholeAct1();
    void manholeAct2();
    
    cocos2d::ui::Button* _clearButton;
    cocos2d::Sprite* _manhole;
    cocos2d::Sprite* _ball;
    cocos2d::Sprite* _ballShadow;
    cocos2d::Sprite* _secret;
    
    cocos2d::Point _manholePosition;
    cocos2d::Point _ballPosition;
    float _accX;
    float _ballSpeedX;
    MANHOLE_STATUS _manholeStatus;
    SECRET_BTN_STATUS _btnStatus;
};

#endif /* defined(__STAGE_04_H__) */

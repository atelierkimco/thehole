#ifndef __STAGE_07_H__
#define __STAGE_07_H__

#include "cocos2d.h"
#include "StageBaseScene.h"

class Stage07 : public StageBaseScene
{
public:
    static cocos2d::Scene* createScene();
    virtual bool init();
    CREATE_FUNC(Stage07);
private:
    enum LAYER_LAP
    {
        SECTION = 0,
        WALL,
        ROOM,
        SWITCH,
        SECRET,
        OJISAN,
        LIGHT,
        CLEAR_BTN,
        SECRET_BTN
    };
    enum OJI_STATUS
    {
        OJI_SIT_DOWN = 0,
        OJI_CATCH,
        OJI_PUT_UP,
        OJI_DAZZLING,
        OJI_FALL,
        OJI_HANG,
        OJI_SECRET_JUMP
    };
    enum LIGHT_STATUS
    {
        LIGHT_ON = 0,
        LIGHT_OFF
    };
    enum SECRET_STAUS
    {
        SECRET_HIDDEN = 0,
        SECRET_FALL
    };
    bool onTouchBegan(cocos2d::Touch* touch, cocos2d::Event* event);
    void onTouchMoved(cocos2d::Touch* touch, cocos2d::Event* event);
    void onTouchEnded(cocos2d::Touch* touch, cocos2d::Event* event);
    virtual void update(float dt);
    
    void setOjiStatus(OJI_STATUS status);
    void switchLightStatus();
    void ojiAnimation();
//    void ojiSecretMove();
    void btnEnabled(cocos2d::ui::Button* btn, bool enable);
    void touchEvent(Ref* pSender, cocos2d::ui::Widget::TouchEventType type);
    
    cocos2d::Sprite* _oji;
    cocos2d::Sprite* _moveWall;
    cocos2d::Sprite* _light;
    cocos2d::Sprite* _switch;
    cocos2d::Sprite* _secret;
    cocos2d::ui::Button* _clearButton;
    cocos2d::ui::Button* _secretButton;
    cocos2d::Rect _hangRect;
    
    float _ojiSpeedY;
    cocos2d::Point _ojiCatchPoint;
    OJI_STATUS _ojiStatus;
    LIGHT_STATUS _lightStatus;
    SECRET_STAUS _secretStatus;
};

#endif /* defined(__STAGE_07_H__) */

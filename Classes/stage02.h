#ifndef __STAGE_02_H__
#define __STAGE_02_H__

#include "cocos2d.h"
#include "StageBaseScene.h"

class Stage02 : public StageBaseScene
{
public:
    static cocos2d::Scene* createScene();
    virtual bool init();
    CREATE_FUNC(Stage02);
private:
    enum LAP
    {
        ROOM = 0,
        CLEAR_BTN,
        HOLE_COVER,
        SECRET,
        SECRET_COVER,
        SECRET_BTN
    };
    enum SECRET_BTN_STATUS
    {
        SECRET_HIDDEN = 0,
        SHOW_SECRET,
    };
    
    bool onTouchBegan(cocos2d::Touch* touch, cocos2d::Event* event);
    void onTouchMoved(cocos2d::Touch* touch, cocos2d::Event* event);
    void onTouchEnded(cocos2d::Touch* touch, cocos2d::Event* event);
    
    void touchEvent(Ref* pSender, cocos2d::ui::Widget::TouchEventType type);
    
    void clearAction();
    void secretAction();
    
    cocos2d::Sprite* _holeCover;
    cocos2d::Sprite* _secret;
    cocos2d::Sprite* _secretCover;
    cocos2d::ui::Button* _clearButton;
    
    float _coverPositionX;
    float _touchBeganPointX;
    bool _coverMoveFlag;
    bool _secretFlag;
    SECRET_BTN_STATUS _btnStatus;
};

#endif /* defined(__STAGE_02_H__) */

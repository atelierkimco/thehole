#ifndef __MyCppGame__StageBase__
#define __MyCppGame__StageBase__

#include "cocos2d.h"
#include "HomeScene.h"
#include "ReplaceStage.h"
#include <ui/UIButton.h>

static const int PARENT_LAYER_TAG = 1000;

enum COMMON_LAYER_LAP
{
    STAGE_LAYER = 0,
    SECRET_LAYER,
    SHADE,
    HEADER,
    HEADER_BTN,
    FOOTER
};
enum BTN_ACTION
{
    TO_HOME_ACT = 0,
    RETRY_ACT,
    STAGE_CLEAR_ACT,
    STAGE_SECRET
};

class StageBaseScene : public cocos2d::Layer
{
public:
    void setCommon(STAGE stageNo);
    
    void touchEvent(Ref* pSender, cocos2d::ui::Widget::TouchEventType type);
    void repraceNextStage(cocos2d::Point holePosition);
    
    void startDirect();
    void makeSecretBtn();
    void getSecret();
    void gatSecret();
    
    cocos2d::Layer* _stageLayer;
    cocos2d::Sprite* _shadeSprite;
    cocos2d::ui::Button* _stageSecret;
    cocos2d::Point _getItemPosition;
    bool _repraceFlag;
    STAGE _stageNo;
    
private:
    void repraceHome();
    void retryStage();
    void watchSecret();
};

#endif /* defined(__MyCppGame__StageBase__) */

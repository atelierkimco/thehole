#ifndef __STAGE_03_H__
#define __STAGE_03_H__

#include "cocos2d.h"
#include "StageBaseScene.h"

class Stage03 : public StageBaseScene
{
public:
    static cocos2d::Scene* createScene();
    virtual bool init();
    CREATE_FUNC(Stage03);
private:
    enum LAYER_LAP
    {
        ROOM = 0,
        HOLE_COVER,
        HAMMER,
        SECRET,
        PICTURE,
        ITEM_BTN,
        CLEAR_BTN,
        SECRET_BTN
    };
    enum BUTTON_ACTION
    {
        ITEM_ACT = 0,
        SECRET_ACT
    };
    enum ITEM_BUTTON_STATUS
    {
        ITEM_FALL = 0,
        ITEM_UNUSE,
        ITEM_USE
    };
    
    bool onTouchBegan(cocos2d::Touch* touch, cocos2d::Event* event);
    void touchEvent(Ref* pSender, cocos2d::ui::Widget::TouchEventType type);
    
    void shakeMove();
    void leanFlame(float tm);
    void holeInEnabled(float tm);
    void fellFlame();
    
    
    cocos2d::Sprite* _hole;
    cocos2d::Sprite* _hammer;
    cocos2d::Sprite* _item;
//    cocos2d::Sprite* _secret;
    cocos2d::Sprite* _picture[3];
    cocos2d::ui::Button* _clearButton;
    cocos2d::ui::Button* _itemButton;
    cocos2d::ui::Button* _secretButton;
    
    int _holeHitCount;
    int _hitCount;
    ITEM_BUTTON_STATUS _hammerStatus;
};

#endif /* defined(__STAGE_03_H__) */

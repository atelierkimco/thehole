#ifndef __MyCppGame__GameClearLayer__
#define __MyCppGame__GameClearLayer__

#include "cocos2d.h"
#include "StageBaseScene.h"

class GameClearScene : public StageBaseScene
{
public:
    static cocos2d::Scene* createScene();
    virtual bool init();
    CREATE_FUNC(GameClearScene);
private:
    enum LAYER_LAP
    {
        BACKGROUND = 0,
        HOME_BTN,
        SHADE,
        HEADER,
        HEADER_BTN,
        FOOTER
    };
    
    void loadSecret();
    void normalClear(float tm);
    void getAllSecretClear(float tm);
    void makeHomeBtn();
    
    cocos2d::ui::Button* _homeButton;
    int _secretCount;
};

#endif /* defined(__MyCppGame__GameClearLayer__) */

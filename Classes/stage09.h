#ifndef __STAGE_09_H__
#define __STAGE_09_H__

#include "cocos2d.h"
#include "StageBaseScene.h"

class Stage09 : public StageBaseScene
{
public:
    static cocos2d::Scene* createScene();
    virtual bool init();
    CREATE_FUNC(Stage09);
private:
    enum LAYER_LAP
    {
        COVER = 0,
        OJISAN,
        SHUTTER,
        SECRET,
        ROOM,
        EXHAUST_PORT,
        CLEAR_BTN,
        SHUTTER_BTN,
        SECRET_BTN
    };
    enum COVER_STATUS
    {
        NORMAL = 0,
        PUSH_DOWN,
        OPEN
    };
    enum OJI_STATUS
    {
        HIDDEN = 0,
        LEFT,
        RIGHT,
        END
    };
    enum BUTTON_STATUS
    {
        SHUTTER_ACT = 0,
        SECRET_ACT
    };
    enum SHUTTER_BTN_STATUS
    {
        HIDDEN_BTN = 0,
        UNPUSH,
        PUSH
    };
    enum SHUTTER_STATUS
    {
        SHUTTER_OPEN = 0,
        APPEAR_SECRET,
        SHUTTER_CLOSE
    };
    
    void onAcceleration(cocos2d::Acceleration* acc, cocos2d::Event* unused_event);
    bool onTouchBegan(cocos2d::Touch* touch, cocos2d::Event* event);
    void touchEvent(Ref* pSender, cocos2d::ui::Widget::TouchEventType type);
    virtual void update(float dt);
    void touchCover();
    void resetCover();
    
    cocos2d::Sprite* _cover;
    cocos2d::Sprite* _oji;
    cocos2d::Sprite* _shutter;
    cocos2d::Sprite* _secret;
    cocos2d::Sprite* _shutterBtn;
    cocos2d::Sprite* _exhaustPort;
    cocos2d::ui::Button* _clearButton;
    cocos2d::ui::Button* _shutterButton;
    cocos2d::ui::Button* _secretButton;
    
    cocos2d::Point _touchBeganPoint;
    cocos2d::Point _coverPosition;
    COVER_STATUS _coverStatus;
    OJI_STATUS _ojiStatus;
    SHUTTER_BTN_STATUS _shutterBtnStatus;
    SHUTTER_STATUS _shutterStatus;
    
    float _accX;
    float _accY;
    float _accZ;
    float _goalAngle;
    float _btnRotateSpeed;
    float _shutterMoveY;
};

#endif /* defined(__STAGE_09_H__) */

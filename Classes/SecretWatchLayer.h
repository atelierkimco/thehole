#ifndef __MyCppGame__SecretWatchLayer__
#define __MyCppGame__SecretWatchLayer__

#include "cocos2d.h"
#include "ReplaceStage.h"
#include <ui/UIButton.h>

class SecretWatchLayer : public cocos2d::LayerColor
{
public:
    virtual bool init();
    CREATE_FUNC(SecretWatchLayer);
    
    void setStageNo(STAGE stage);
    
private:
    enum SPRITE_LAP
    {
        SECRET_IMG = 0,
        GET,
        CLOSE
    };
    bool onTouchBegan(cocos2d::Touch* touch, cocos2d::Event* event);
    void touchEvent(Ref* pSender, cocos2d::ui::Widget::TouchEventType type);
    
    void startAction();
    void makeCloseBtn();
    void endAction();
    
    cocos2d::Sprite* _secretSprite;
    cocos2d::ui::Button* _closeButton;
    
    STAGE _secretNo;
};

#endif /* defined(__MyCppGame__SecretWatchLayer__) */

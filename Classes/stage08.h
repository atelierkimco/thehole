#ifndef __STAGE_08_H__
#define __STAGE_08_H__

#include "cocos2d.h"
#include "StageBaseScene.h"

class Stage08 : public StageBaseScene
{
public:
    static cocos2d::Scene* createScene();
    virtual bool init();
    CREATE_FUNC(Stage08);
private:
    enum LAYER_LAP
    {
        ROOM = 0,
        OJISAN_1, //壁に突っ込んだ後のおじさん画像
        SAND_BAG,
        OJISAN_0,
        GLOVE,
        SECRET,
        ITEM_BTN,
        CLEAR_BTN,
        SECRET_BTN
    };
    enum BUTTON_ACTION
    {
        ITEM_ACT = 0,
        SECRET_ACT
    };
    enum ITEM_BUTTON_STATUS
    {
        ITEM_FALL = 0,
        ITEM_UNUSE,
        ITEM_USE
    };
    enum SECRET_STATUS
    {
        FIRST_STEP = 0,
        START,
        LIMIT
    };
    bool onTouchBegan(cocos2d::Touch* touch, cocos2d::Event* event);
    void touchEvent(Ref* pSender, cocos2d::ui::Widget::TouchEventType type);
    
    void getItem();
    void ojiPunch();
    void ojiHit();
    void timeLimit(float tm);
    void secretBtnEnabled();
    
    cocos2d::Sprite* _sandBag;
    cocos2d::Sprite* _glove;
    cocos2d::Sprite* _item;
    cocos2d::Sprite* _ojiHole;
    cocos2d::Sprite* _oji;
    cocos2d::Sprite* _secret;
    cocos2d::ui::Button* _clearButton;
    cocos2d::ui::Button* _itemButton;
    cocos2d::ui::Button* _secretButton;
    
    int _hitCount;
    ITEM_BUTTON_STATUS _gloveStatus;
    SECRET_STATUS _secretStatus;
};

#endif /* defined(__STAGE_08_H__) */
